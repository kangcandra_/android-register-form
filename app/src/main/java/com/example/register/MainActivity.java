package com.example.register;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import android.graphics.Color;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    TextView et_tanggal_lahir;
    Button register_button;
    EditText et_nik, et_nama, et_tempat_lahir, et_alamat;
    //jenis kelamin
    RadioGroup rd_jenis_kelamin;
    RadioButton lakilaki, perempuan;

    Spinner sp_pekerjaan, sp_status_perkawinan;

    String nik;
    String nama;
    String tanggal_lahir;
    String tempat_lahir;
    String alamat;
    String jenis_kelamin;
    String pekerjaan;
    String status_perkawinan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_tanggal_lahir = findViewById(R.id.et_tanggal_lahir);

        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                TextView tanggal = findViewById(R.id.et_tanggal_lahir);
                String myFormat = "dd-MMMM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                tanggal.setText(sdf.format(myCalendar.getTime()));
            }
        };

        et_tanggal_lahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MainActivity.this, date,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        //send data
        register_button = (Button) findViewById(R.id.register_button);
        et_nik = (EditText) findViewById(R.id.et_nik);
        et_nama = (EditText) findViewById(R.id.et_nama);
        et_tempat_lahir = (EditText) findViewById(R.id.et_tempat_lahir);
        et_tanggal_lahir = (EditText) findViewById(R.id.et_tanggal_lahir);
        et_alamat = (EditText) findViewById(R.id.et_alamat);
        sp_pekerjaan = (Spinner)findViewById(R.id.sp_kerja);
        sp_status_perkawinan = (Spinner)findViewById(R.id.sp_status_perkawinan);

        rd_jenis_kelamin = (RadioGroup) findViewById(R.id.rd_jenis_kelamin);
        lakilaki = (RadioButton) findViewById(R.id.lakilaki);
        perempuan = (RadioButton) findViewById(R.id.perempuan);



        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nama = et_nama.getText().toString();
                nik = et_nik.getText().toString();
                tempat_lahir = et_tempat_lahir.getText().toString();
                tanggal_lahir = et_tanggal_lahir.getText().toString();
                alamat = et_alamat.getText().toString();
                pekerjaan = sp_pekerjaan.getSelectedItem().toString();
                status_perkawinan = sp_status_perkawinan.getSelectedItem().toString();

                //jenis kelamin
                int selectedId = rd_jenis_kelamin.getCheckedRadioButtonId();
                if (selectedId == lakilaki.getId()){
                    jenis_kelamin = "Laki-laki";
                } else if (selectedId == perempuan.getId()){
                    jenis_kelamin = "Perempuan";
                }


                Intent i = new Intent(MainActivity.this,Profile.class);
                // validasi
                if(nama.equals("") || nik.equals("") || tempat_lahir.equals("") || tanggal_lahir.equals("")){
                    Toast.makeText(MainActivity.this, "Data Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
                } else if(nik.length()> 16 || nik.length() < 16){
                    Toast.makeText(MainActivity.this, "NIK Wajib 16 Digit!", Toast.LENGTH_LONG).show();
                }
                else{
                    i.putExtra("nama",nama);
                    i.putExtra("nik",nik);
                    i.putExtra("tanggal_lahir",tanggal_lahir);
                    i.putExtra("tempat_lahir",tempat_lahir);
                    i.putExtra("alamat",alamat);
                    i.putExtra("jenis_kelamin",jenis_kelamin);
                    i.putExtra("pekerjaan",pekerjaan);
                    i.putExtra("status_perkawinan",status_perkawinan);
                    startActivity(i);
                }


            }
        });


    }


}
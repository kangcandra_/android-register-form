package com.example.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Profile extends AppCompatActivity {


    TextView tv_nama,tv_nik, tv_tempat_lahir, tv_tanggal_lahir, tv_alamat, tv_jenis_kelamin, tv_pekerjaan, tv_status_perkawinan;
    String nama,nik, tempat_lahir, tanggal_lahir, alamat, pekerjaan, jenis_kelamin, status_perkawinan;
    Button back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        tv_nama = (TextView) findViewById(R.id.tv_nama);
        tv_nik = (TextView) findViewById(R.id.tv_nik);
        tv_tempat_lahir = (TextView) findViewById(R.id.tv_tempat_lahir);
        tv_tanggal_lahir = (TextView) findViewById(R.id.tv_tanggal_lahir);
        tv_alamat = (TextView) findViewById(R.id.tv_alamat);
        tv_jenis_kelamin = (TextView) findViewById(R.id.tv_jenis_kelamin);
        tv_pekerjaan = (TextView) findViewById(R.id.tv_pekerjaan);
        tv_status_perkawinan= (TextView) findViewById(R.id.tv_status_perkawinan);

        if(getIntent().getStringExtra("nama") != null){
            nama = getIntent().getStringExtra("nama");
            tv_nama.setText(nama);
        }
        if(getIntent().getStringExtra("nik") != null){
            nik = getIntent().getStringExtra("nik");
            tv_nik.setText(nik);
        }

        if(getIntent().getStringExtra("tempat_lahir") != null){
            tempat_lahir = getIntent().getStringExtra("tempat_lahir");
            tv_tempat_lahir.setText(tempat_lahir);
        }

        if(getIntent().getStringExtra("tanggal_lahir") != null){
            tanggal_lahir = getIntent().getStringExtra("tanggal_lahir");
            tv_tanggal_lahir.setText(tanggal_lahir);
        }

        if(getIntent().getStringExtra("alamat") != null){
            alamat = getIntent().getStringExtra("alamat");
            tv_alamat.setText(alamat);
        }

        if(getIntent().getStringExtra("jenis_kelamin") != null){
            jenis_kelamin = getIntent().getStringExtra("jenis_kelamin");
            tv_jenis_kelamin.setText(jenis_kelamin);
        }

        if(getIntent().getStringExtra("pekerjaan") != null){
            pekerjaan = getIntent().getStringExtra("pekerjaan");
            tv_pekerjaan.setText(pekerjaan);
        }

        if(getIntent().getStringExtra("status_perkawinan") != null){
            status_perkawinan = getIntent().getStringExtra("status_perkawinan");
            tv_status_perkawinan.setText(status_perkawinan);
        }

        // back button
        back_button = (Button) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this,MainActivity.class);
                startActivity(i);
                finish();

            }
        });
    }
}